package com.ruben;

import javax.annotation.Resource;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort;
import org.springframework.data.r2dbc.core.R2dbcEntityTemplate;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.data.relational.core.query.Query;
import org.springframework.data.relational.core.query.Update;

import com.ruben.pojo.Person;
import com.ruben.repository.PersonRepository;

import io.r2dbc.spi.ConnectionFactory;
import lombok.SneakyThrows;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@SpringBootTest
class SimpleR2dbcApplicationTests {

    @Resource
    private PersonRepository personRepository;

    @Resource
    private ConnectionFactory connectionFactory;

    @Test
    @SneakyThrows
    void contextLoads() {
        Assertions.assertAll(() -> {
            final R2dbcEntityTemplate template = new R2dbcEntityTemplate(connectionFactory);
            Mono<Integer> update = template.update(Person.class)
                    .inTable("PERSON").matching(Query.query(Criteria.where("firstname").is("John")))
                    .apply(Update.update("age", 42));
            Flux<Person> all = template.select(Person.class).matching(Query.query(Criteria
                            .where("firstname").is("John")
                            .and("lastname").in("Doe", "White"))
                    .sort(Sort.by(Sort.Order.desc("id")))).all();
            update.then(all.collectList()).log().block();
        });
    }

    @Test
    void test() {
        Assertions.assertAll(() -> {
            personRepository.setFixedFirstnameFor("John", "Doe")
                    .then(personRepository.findByFirstname("John").collectList())
                    .then(personRepository.findByQueryWithExpression("Doe").collectList()).log()
                    .block();
        });
    }

}
