DELETE
FROM PERSON;

INSERT INTO PERSON (id, firstname, age, lastname)
VALUES (1, 'John', 18, 'White'),
       (2, 'John', 20, 'Doe'),
       (3, 'Tom', 28, 'clancy'),
       (4, 'Sandy', 21, 'white'),
       (5, 'Billie', 24, 'Billie');