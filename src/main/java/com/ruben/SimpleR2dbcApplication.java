package com.ruben;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpleR2dbcApplication {

    public static void main(String[] args) {
        SpringApplication.run(SimpleR2dbcApplication.class, args);
    }

}
