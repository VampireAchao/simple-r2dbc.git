package com.ruben.pojo;

import java.io.Serializable;

import lombok.Data;

/**
 * @author <achao1441470436@gmail.com>
 * @since 2022/5/14 15:03
 */
@Data
public class Person implements Serializable {

    private static final long serialVersionUID = -5182607516562964503L;

    private String firstname;
    private String lastname;
    private Integer age;


}
