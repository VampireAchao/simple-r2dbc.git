package com.ruben.repository;

import org.springframework.data.r2dbc.repository.Modifying;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

import com.ruben.pojo.Person;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface PersonRepository extends ReactiveCrudRepository<Person, String> {

    Flux<Person> findByFirstname(String firstname);

    @Modifying
    @Query("UPDATE person SET firstname = :firstname where lastname = :lastname")
    Mono<Integer> setFixedFirstnameFor(String firstname, String lastname);

    @Query("SELECT * FROM person WHERE lastname = :#{[0]}")
    Flux<Person> findByQueryWithExpression(String lastname);

}